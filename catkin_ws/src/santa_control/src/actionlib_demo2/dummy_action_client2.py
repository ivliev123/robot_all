import rospy
import actionlib
import traceback
from std_msgs.msg import String
from actionlib_demo.msg import DoSomethingAction, DoSomethingActionFeedback, DoSomethingActionGoal, \
    DoSomethingActionResult, DoSomethingFeedback, DoSomethingGoal, DoSomethingResult

from aruco_move.msg import aruco_moveAction, aruco_moveActionFeedback, aruco_moveActionGoal, \
    aruco_moveActionResult, aruco_moveFeedback, aruco_moveGoal, aruco_moveResult

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionGoal, MoveBaseActionFeedback
from math import radians, degrees
from actionlib_msgs.msg import *

from geometry_msgs.msg import Point
import tf
from geometry_msgs.msg import Pose, Quaternion, Point, PoseStamped, PoseWithCovarianceStamped

import geometry_msgs.msg

class DummyActionClient2(object):
    def __init__(self):

        #define a client for to send goal requests to the move_base server through a SimpleActionClient
        self.ac = actionlib.SimpleActionClient("move_base", MoveBaseAction)

        self.do_something_client = actionlib.SimpleActionClient('aruco_move', aruco_moveAction)

        self.trigger_one_subscriber = rospy.Subscriber('trigger_one', String, self._trigger_one)

        self.other_topic_one_subscriber = rospy.Subscriber('other_topic_one', String, self._other_topic_one)
        self.goal_id = 1
        self.auto_plan = False
        self.auto_plan_state = 0

    def start(self):
        self._loginfo('Action Client Started2')
        self.do_something_client.wait_for_server(rospy.Duration.from_sec(15))
        self.ac.wait_for_server(rospy.Duration.from_sec(5.0))

        #wait for the action server to come up
        #while(not ac.wait_for_server(rospy.Duration.from_sec(5.0))):
        #      rospy.loginfo("Waiting for the move_base action server to come up")

    def stop(self):
        self._loginfo('Action Client Stopped')
        self.do_something_client = None
    
    def moveToGoal(self,xGoal,yGoal):
      goal = MoveBaseGoal()

      #set up the frame parameters
      goal.target_pose.header.frame_id = "base_footprint"
      goal.target_pose.header.stamp = rospy.Time.now()

      # moving towards the goal*/

      goal.target_pose.pose.position =  Point(xGoal,yGoal,0)
      goal.target_pose.pose.orientation.x = 0.0
      goal.target_pose.pose.orientation.y = 0.0
      goal.target_pose.pose.orientation.z = 0.0
      goal.target_pose.pose.orientation.w = 1.0

      rospy.loginfo("Sending goal location ...")
      self.ac.send_goal(goal)

      self.ac.wait_for_result(rospy.Duration(60))

      if(self.ac.get_state() ==  GoalStatus.SUCCEEDED):
              rospy.loginfo("You have reached the destination")
              return True

      else:
              rospy.loginfo("The robot failed to reach the destination")
              return False
    def cancel_goal(self):
        self.ac.cancel_goal()
        self.do_something_client.cancel_goal()
        if (self.auto_plan == True):
            self.auto_plan = False

    def send_goal_move(self, xGoal,yGoal):
        try:
            goal = MoveBaseGoal()
            #set up the frame parameters
            goal.target_pose.header.frame_id = "base_footprint"
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.goal_id.id = "1"

            # moving towards the goal*/
            goal.target_pose.pose.position =  Point(xGoal,yGoal,0)
            goal.target_pose.pose.orientation.x = 0.0
            goal.target_pose.pose.orientation.y = 0.0
            goal.target_pose.pose.orientation.z = 0.0
            goal.target_pose.pose.orientation.w = 1.0

            self.ac.send_goal(goal,
                                               active_cb=self._goal_move_active,
                                               feedback_cb=self._goal_move_feedback,
                                               done_cb=self._goal_move_done)

            self._loginfo('goal move has been sent')

        except Exception as e:
            print(e.message)
            traceback.print_exc()
            self._loginfo('Error sending goal move')
            # self.ac.wait_for_server(rospy.Duration.from_sec(15))
            # self.send_goal_one(length)

    def send_goal_move_map(self,goal_id,xGoal,yGoal,heading = None):
        try:
            goal = MoveBaseGoal()
            #set up the frame parameters
            goal.target_pose.header.frame_id = "map"
            t = rospy.Time.now()
            goal.target_pose.header.stamp = t
            self.goal_id = goal_id

            # moving towards the goal*/
            goal.target_pose.pose.position =  Point(xGoal,yGoal,0)
            # goal.target_pose.pose.orientation.x = 0.0
            # goal.target_pose.pose.orientation.y = 0.0
            # goal.target_pose.pose.orientation.z = 0.0
            # goal.target_pose.pose.orientation.w = 1.0

            if (heading != None) :
                q = tf.transformations.quaternion_from_euler(0, 0, heading)
                goal.target_pose.pose.orientation = geometry_msgs.msg.Quaternion(*q)

            self.ac.send_goal(goal,
                                               active_cb=self._goal_move_active,
                                               feedback_cb=self._goal_move_feedback,
                                               done_cb=self._goal_move_done)

            self._loginfo('goal move has been sent')

        except Exception as e:
            print(e.message)
            traceback.print_exc()
            self._loginfo('Error sending goal move')
            # self.ac.wait_for_server(rospy.Duration.from_sec(15))
            # self.send_goal_one(length)

    def send_goal_move_auto(self):
        if (self.auto_plan == False):
            self.auto_plan = True
            self.send_goal_move_map(1,-5,0,3.14)
            self.auto_plan_state = 1            

    def send_goal_one(self, length):
        try:
            goal = aruco_moveGoal()
            goal.aruco_id = length
            self.do_something_client.send_goal(goal,
                                               active_cb=self._goal_one_active,
                                               feedback_cb=self._goal_one_feedback,
                                               done_cb=self._goal_one_done)

            self._loginfo('goal one has been sent')

        except Exception as e:
            print(e.message)
            traceback.print_exc()
            self._loginfo('Error sending goal one')
            self.do_something_client.wait_for_server(rospy.Duration.from_sec(15))
            self.send_goal_one(length)
    
    def _goal_move_active(self):
        self._loginfo('goal move has transitioned to active state')

    def _goal_move_feedback(self, feedback):
        # type: (DoSomethingFeedback) -> None
        self._loginfo('Goal move feedback received: {}'.format(feedback))

    def _goal_move_done(self, state, result):
        # type: (actionlib.GoalStatus, DoSomethingResult) -> None
        self._loginfo('Goal move done callback triggered')
        self._loginfo(str(state))
        self._loginfo(str(result))
        if (self.auto_plan == True):
            if (self.auto_plan_state == 1):
                self.auto_plan_state = 2
                self.send_goal_one(2)
            if (self.auto_plan_state == 3):
                self.auto_plan_state = 4
                self.send_goal_one(1)

    def _goal_one_active(self):
        self._loginfo('goal one has transitioned to active state')

    def _goal_one_feedback(self, feedback):
        # type: (DoSomethingFeedback) -> None
        self._loginfo('Goal one feedback received: {}'.format(feedback))

    def _goal_one_done(self, state, result):
        # type: (actionlib.GoalStatus, DoSomethingResult) -> None
        self._loginfo('Goal one done callback triggered')
        self._loginfo(str(state))
        self._loginfo(str(result))
        self._loginfo(self.goal_id)
        # self._loginfo('Do something result: ' + str(result.did_finish_doing_something))
        # if result.aruco_id == 1:
        #     self.send_goal_one(2)
        # if result.aruco_id == 2:
        #     self.send_goal_one(3)
        # if result.aruco_id == 3:
        #     self.send_goal_one(1)
        if (self.auto_plan == True):
            if (self.auto_plan_state == 2):
                self.auto_plan_state = 3
                self.send_goal_move_map(2,0,0,0)
            if (self.auto_plan_state == 4):
                self.auto_plan_state = 1
                self.send_goal_move_map(1,-5,0,3.14)
        


    def _trigger_one(self, data):
        self._loginfo('Trigger one called')
        self._loginfo(data)

        self.send_goal_one(1)

    def _other_topic_one(self, data):
        self._loginfo('other_topic_one called')
        self._loginfo(data)
        self._loginfo('Preempting action do_something goal')
        self.do_something_client.cancel_goal()

    @staticmethod
    def _loginfo(message):
        # type: (str) -> None

        rospy.loginfo('DummyActionClient ({}) {}'.format('dummy_client', message))