#!/usr/bin/env python
import rospy
from actionlib_demo2 import DummyActionClient2


def start_actionlib_demo():

    rospy.init_node('dummy_action_client', anonymous=False)  # initialize ros node

    dummy_action_client = DummyActionClient2()
    dummy_action_client.start()

    def stop_dummy_action_client():
        dummy_action_client.stop()

    rospy.on_shutdown(stop_dummy_action_client)

    def choose():
		choice='q'
		rospy.loginfo("|-------------------------------|")
		rospy.loginfo("|PRESSE A KEY:")
		rospy.loginfo("|'1': Office 1 ")
		rospy.loginfo("|'2': Office 2 ")
		rospy.loginfo("|'3': Office 3 ")
		rospy.loginfo("|'q': Quit ")
		rospy.loginfo("|-------------------------------|")
		rospy.loginfo("|WHERE TO GO?")
		choice = input()
		return choice
    
    choice = 't'
    
    while choice != 'q':
			choice = choose()
			if (choice == 1):

				dummy_action_client.send_goal_one(1)

			elif (choice == 2):
		
				dummy_action_client.send_goal_one(2)
		
			elif (choice == 3):

				dummy_action_client.moveToGoal(0.2,0.0)

			elif (choice == 4):

				dummy_action_client.moveToGoal(-0.2,0.0)

			elif (choice == 8):

				dummy_action_client.moveToGoal(1,0.0)

			elif (choice == 5):

				dummy_action_client.moveToGoal(-1,0.0)

			elif (choice == 9):

				dummy_action_client.send_goal_move_map(1,-5,0,3.14)

			elif (choice == 7):

				dummy_action_client.send_goal_move_map(2,0,0,0)

			elif (choice == 0):

				dummy_action_client.cancel_goal()
			
			elif (choice == 6):

				dummy_action_client.send_goal_move_auto()

			#if (choice!='q'):

				#if (self.goalReached):
				#	rospy.loginfo("Congratulations!")
					#rospy.spin()

                #else:
				#	rospy.loginfo("Hard Luck!")
                #rospy.loginfo("Hard Luck!")

    rospy.spin()  # spin() simply keeps python from exiting until this node is stopped


if __name__ == '__main__':
    start_actionlib_demo()