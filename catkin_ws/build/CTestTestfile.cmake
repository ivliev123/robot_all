# CMake generated Testfile for 
# Source directory: /home/ubuntu/catkin_ws/src
# Build directory: /home/ubuntu/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(santa_msgs)
subdirs(turtlebot3/turtlebot3)
subdirs(turtlebot3_msgs)
subdirs(turtlebot3/turtlebot3_navigation)
subdirs(test_python)
subdirs(santa_control)
subdirs(baxter_core_msgs)
subdirs(baxter_face)
subdirs(gscam)
subdirs(rplidar_ros)
subdirs(laser_filters)
subdirs(aruco_move)
subdirs(turtlebot3/turtlebot3_bringup)
subdirs(turtlebot3/turtlebot3_example)
subdirs(turtlebot3/turtlebot3_slam)
subdirs(turtlebot3/turtlebot3_teleop)
subdirs(hypharos_minibot)
subdirs(santa_bringup)
subdirs(turtlebot3/turtlebot3_description)
subdirs(ydlidar)
