# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "aruco_move: 7 messages, 0 services")

set(MSG_I_FLAGS "-Iaruco_move:/home/ubuntu/catkin_ws/devel/share/aruco_move/msg;-Iactionlib_msgs:/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(aruco_move_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" ""
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" "aruco_move/aruco_moveFeedback:aruco_move/aruco_moveGoal:actionlib_msgs/GoalStatus:aruco_move/aruco_moveActionFeedback:aruco_move/aruco_moveActionResult:aruco_move/aruco_moveResult:aruco_move/aruco_moveActionGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" ""
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" ""
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" "aruco_move/aruco_moveGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" "aruco_move/aruco_moveFeedback:actionlib_msgs/GoalID:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_custom_target(_aruco_move_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "aruco_move" "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" "actionlib_msgs/GoalID:std_msgs/Header:aruco_move/aruco_moveResult:actionlib_msgs/GoalStatus"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)
_generate_msg_cpp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
)

### Generating Services

### Generating Module File
_generate_module_cpp(aruco_move
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(aruco_move_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(aruco_move_generate_messages aruco_move_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_cpp _aruco_move_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(aruco_move_gencpp)
add_dependencies(aruco_move_gencpp aruco_move_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS aruco_move_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)
_generate_msg_eus(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
)

### Generating Services

### Generating Module File
_generate_module_eus(aruco_move
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(aruco_move_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(aruco_move_generate_messages aruco_move_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_eus _aruco_move_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(aruco_move_geneus)
add_dependencies(aruco_move_geneus aruco_move_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS aruco_move_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)
_generate_msg_lisp(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
)

### Generating Services

### Generating Module File
_generate_module_lisp(aruco_move
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(aruco_move_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(aruco_move_generate_messages aruco_move_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_lisp _aruco_move_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(aruco_move_genlisp)
add_dependencies(aruco_move_genlisp aruco_move_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS aruco_move_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)
_generate_msg_nodejs(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
)

### Generating Services

### Generating Module File
_generate_module_nodejs(aruco_move
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(aruco_move_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(aruco_move_generate_messages aruco_move_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_nodejs _aruco_move_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(aruco_move_gennodejs)
add_dependencies(aruco_move_gennodejs aruco_move_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS aruco_move_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)
_generate_msg_py(aruco_move
  "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
)

### Generating Services

### Generating Module File
_generate_module_py(aruco_move
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(aruco_move_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(aruco_move_generate_messages aruco_move_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveAction.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionGoal.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionFeedback.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ubuntu/catkin_ws/devel/share/aruco_move/msg/aruco_moveActionResult.msg" NAME_WE)
add_dependencies(aruco_move_generate_messages_py _aruco_move_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(aruco_move_genpy)
add_dependencies(aruco_move_genpy aruco_move_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS aruco_move_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/aruco_move
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(aruco_move_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(aruco_move_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/aruco_move
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(aruco_move_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(aruco_move_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/aruco_move
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(aruco_move_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(aruco_move_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/aruco_move
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(aruco_move_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(aruco_move_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/aruco_move
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(aruco_move_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(aruco_move_generate_messages_py std_msgs_generate_messages_py)
endif()
