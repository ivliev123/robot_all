
"use strict";

let aruco_moveActionFeedback = require('./aruco_moveActionFeedback.js');
let aruco_moveFeedback = require('./aruco_moveFeedback.js');
let aruco_moveAction = require('./aruco_moveAction.js');
let aruco_moveResult = require('./aruco_moveResult.js');
let aruco_moveGoal = require('./aruco_moveGoal.js');
let aruco_moveActionResult = require('./aruco_moveActionResult.js');
let aruco_moveActionGoal = require('./aruco_moveActionGoal.js');

module.exports = {
  aruco_moveActionFeedback: aruco_moveActionFeedback,
  aruco_moveFeedback: aruco_moveFeedback,
  aruco_moveAction: aruco_moveAction,
  aruco_moveResult: aruco_moveResult,
  aruco_moveGoal: aruco_moveGoal,
  aruco_moveActionResult: aruco_moveActionResult,
  aruco_moveActionGoal: aruco_moveActionGoal,
};
