// Auto-generated. Do not edit!

// (in-package santa_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class PidState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.vx_to = null;
      this.pid_vx = null;
      this.P_v = null;
      this.I_v = null;
      this.D_v = null;
      this.w_to = null;
      this.pid_w = null;
      this.P_w = null;
      this.I_w = null;
      this.D_w = null;
    }
    else {
      if (initObj.hasOwnProperty('vx_to')) {
        this.vx_to = initObj.vx_to
      }
      else {
        this.vx_to = 0.0;
      }
      if (initObj.hasOwnProperty('pid_vx')) {
        this.pid_vx = initObj.pid_vx
      }
      else {
        this.pid_vx = 0.0;
      }
      if (initObj.hasOwnProperty('P_v')) {
        this.P_v = initObj.P_v
      }
      else {
        this.P_v = 0.0;
      }
      if (initObj.hasOwnProperty('I_v')) {
        this.I_v = initObj.I_v
      }
      else {
        this.I_v = 0.0;
      }
      if (initObj.hasOwnProperty('D_v')) {
        this.D_v = initObj.D_v
      }
      else {
        this.D_v = 0.0;
      }
      if (initObj.hasOwnProperty('w_to')) {
        this.w_to = initObj.w_to
      }
      else {
        this.w_to = 0.0;
      }
      if (initObj.hasOwnProperty('pid_w')) {
        this.pid_w = initObj.pid_w
      }
      else {
        this.pid_w = 0.0;
      }
      if (initObj.hasOwnProperty('P_w')) {
        this.P_w = initObj.P_w
      }
      else {
        this.P_w = 0.0;
      }
      if (initObj.hasOwnProperty('I_w')) {
        this.I_w = initObj.I_w
      }
      else {
        this.I_w = 0.0;
      }
      if (initObj.hasOwnProperty('D_w')) {
        this.D_w = initObj.D_w
      }
      else {
        this.D_w = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PidState
    // Serialize message field [vx_to]
    bufferOffset = _serializer.float32(obj.vx_to, buffer, bufferOffset);
    // Serialize message field [pid_vx]
    bufferOffset = _serializer.float32(obj.pid_vx, buffer, bufferOffset);
    // Serialize message field [P_v]
    bufferOffset = _serializer.float32(obj.P_v, buffer, bufferOffset);
    // Serialize message field [I_v]
    bufferOffset = _serializer.float32(obj.I_v, buffer, bufferOffset);
    // Serialize message field [D_v]
    bufferOffset = _serializer.float32(obj.D_v, buffer, bufferOffset);
    // Serialize message field [w_to]
    bufferOffset = _serializer.float32(obj.w_to, buffer, bufferOffset);
    // Serialize message field [pid_w]
    bufferOffset = _serializer.float32(obj.pid_w, buffer, bufferOffset);
    // Serialize message field [P_w]
    bufferOffset = _serializer.float32(obj.P_w, buffer, bufferOffset);
    // Serialize message field [I_w]
    bufferOffset = _serializer.float32(obj.I_w, buffer, bufferOffset);
    // Serialize message field [D_w]
    bufferOffset = _serializer.float32(obj.D_w, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PidState
    let len;
    let data = new PidState(null);
    // Deserialize message field [vx_to]
    data.vx_to = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pid_vx]
    data.pid_vx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [P_v]
    data.P_v = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [I_v]
    data.I_v = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [D_v]
    data.D_v = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_to]
    data.w_to = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pid_w]
    data.pid_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [P_w]
    data.P_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [I_w]
    data.I_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [D_w]
    data.D_w = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 40;
  }

  static datatype() {
    // Returns string type for a message object
    return 'santa_msgs/PidState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'efe518362e45b5e054b6d3e7414a7759';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    ########################################
    # Messages
    ########################################
    float32 vx_to
    float32 pid_vx
    
    float32 P_v
    float32 I_v
    float32 D_v
    
    float32 w_to
    float32 pid_w
    
    float32 P_w
    float32 I_w
    float32 D_w
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PidState(null);
    if (msg.vx_to !== undefined) {
      resolved.vx_to = msg.vx_to;
    }
    else {
      resolved.vx_to = 0.0
    }

    if (msg.pid_vx !== undefined) {
      resolved.pid_vx = msg.pid_vx;
    }
    else {
      resolved.pid_vx = 0.0
    }

    if (msg.P_v !== undefined) {
      resolved.P_v = msg.P_v;
    }
    else {
      resolved.P_v = 0.0
    }

    if (msg.I_v !== undefined) {
      resolved.I_v = msg.I_v;
    }
    else {
      resolved.I_v = 0.0
    }

    if (msg.D_v !== undefined) {
      resolved.D_v = msg.D_v;
    }
    else {
      resolved.D_v = 0.0
    }

    if (msg.w_to !== undefined) {
      resolved.w_to = msg.w_to;
    }
    else {
      resolved.w_to = 0.0
    }

    if (msg.pid_w !== undefined) {
      resolved.pid_w = msg.pid_w;
    }
    else {
      resolved.pid_w = 0.0
    }

    if (msg.P_w !== undefined) {
      resolved.P_w = msg.P_w;
    }
    else {
      resolved.P_w = 0.0
    }

    if (msg.I_w !== undefined) {
      resolved.I_w = msg.I_w;
    }
    else {
      resolved.I_w = 0.0
    }

    if (msg.D_w !== undefined) {
      resolved.D_w = msg.D_w;
    }
    else {
      resolved.D_w = 0.0
    }

    return resolved;
    }
};

module.exports = PidState;
