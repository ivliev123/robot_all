// Auto-generated. Do not edit!

// (in-package santa_bringup.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class my_pid {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.vx_to = null;
      this.pid_vx = null;
      this.error_vx = null;
      this.total_error_vx = null;
      this.d_error_vx = null;
      this.w_to = null;
      this.pid_w = null;
      this.error_w = null;
      this.total_error_w = null;
      this.d_error_w = null;
    }
    else {
      if (initObj.hasOwnProperty('vx_to')) {
        this.vx_to = initObj.vx_to
      }
      else {
        this.vx_to = 0.0;
      }
      if (initObj.hasOwnProperty('pid_vx')) {
        this.pid_vx = initObj.pid_vx
      }
      else {
        this.pid_vx = 0.0;
      }
      if (initObj.hasOwnProperty('error_vx')) {
        this.error_vx = initObj.error_vx
      }
      else {
        this.error_vx = 0.0;
      }
      if (initObj.hasOwnProperty('total_error_vx')) {
        this.total_error_vx = initObj.total_error_vx
      }
      else {
        this.total_error_vx = 0.0;
      }
      if (initObj.hasOwnProperty('d_error_vx')) {
        this.d_error_vx = initObj.d_error_vx
      }
      else {
        this.d_error_vx = 0.0;
      }
      if (initObj.hasOwnProperty('w_to')) {
        this.w_to = initObj.w_to
      }
      else {
        this.w_to = 0.0;
      }
      if (initObj.hasOwnProperty('pid_w')) {
        this.pid_w = initObj.pid_w
      }
      else {
        this.pid_w = 0.0;
      }
      if (initObj.hasOwnProperty('error_w')) {
        this.error_w = initObj.error_w
      }
      else {
        this.error_w = 0.0;
      }
      if (initObj.hasOwnProperty('total_error_w')) {
        this.total_error_w = initObj.total_error_w
      }
      else {
        this.total_error_w = 0.0;
      }
      if (initObj.hasOwnProperty('d_error_w')) {
        this.d_error_w = initObj.d_error_w
      }
      else {
        this.d_error_w = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type my_pid
    // Serialize message field [vx_to]
    bufferOffset = _serializer.float32(obj.vx_to, buffer, bufferOffset);
    // Serialize message field [pid_vx]
    bufferOffset = _serializer.float32(obj.pid_vx, buffer, bufferOffset);
    // Serialize message field [error_vx]
    bufferOffset = _serializer.float32(obj.error_vx, buffer, bufferOffset);
    // Serialize message field [total_error_vx]
    bufferOffset = _serializer.float32(obj.total_error_vx, buffer, bufferOffset);
    // Serialize message field [d_error_vx]
    bufferOffset = _serializer.float32(obj.d_error_vx, buffer, bufferOffset);
    // Serialize message field [w_to]
    bufferOffset = _serializer.float32(obj.w_to, buffer, bufferOffset);
    // Serialize message field [pid_w]
    bufferOffset = _serializer.float32(obj.pid_w, buffer, bufferOffset);
    // Serialize message field [error_w]
    bufferOffset = _serializer.float32(obj.error_w, buffer, bufferOffset);
    // Serialize message field [total_error_w]
    bufferOffset = _serializer.float32(obj.total_error_w, buffer, bufferOffset);
    // Serialize message field [d_error_w]
    bufferOffset = _serializer.float32(obj.d_error_w, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type my_pid
    let len;
    let data = new my_pid(null);
    // Deserialize message field [vx_to]
    data.vx_to = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pid_vx]
    data.pid_vx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [error_vx]
    data.error_vx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [total_error_vx]
    data.total_error_vx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [d_error_vx]
    data.d_error_vx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_to]
    data.w_to = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pid_w]
    data.pid_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [error_w]
    data.error_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [total_error_w]
    data.total_error_w = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [d_error_w]
    data.d_error_w = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 40;
  }

  static datatype() {
    // Returns string type for a message object
    return 'santa_bringup/my_pid';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2c7179894e731f545124da7ece67825e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 vx_to
    float32 pid_vx
    
    float32 error_vx
    float32 total_error_vx
    float32 d_error_vx
    
    
    float32 w_to
    float32 pid_w
    
    float32 error_w
    float32 total_error_w
    float32 d_error_w
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new my_pid(null);
    if (msg.vx_to !== undefined) {
      resolved.vx_to = msg.vx_to;
    }
    else {
      resolved.vx_to = 0.0
    }

    if (msg.pid_vx !== undefined) {
      resolved.pid_vx = msg.pid_vx;
    }
    else {
      resolved.pid_vx = 0.0
    }

    if (msg.error_vx !== undefined) {
      resolved.error_vx = msg.error_vx;
    }
    else {
      resolved.error_vx = 0.0
    }

    if (msg.total_error_vx !== undefined) {
      resolved.total_error_vx = msg.total_error_vx;
    }
    else {
      resolved.total_error_vx = 0.0
    }

    if (msg.d_error_vx !== undefined) {
      resolved.d_error_vx = msg.d_error_vx;
    }
    else {
      resolved.d_error_vx = 0.0
    }

    if (msg.w_to !== undefined) {
      resolved.w_to = msg.w_to;
    }
    else {
      resolved.w_to = 0.0
    }

    if (msg.pid_w !== undefined) {
      resolved.pid_w = msg.pid_w;
    }
    else {
      resolved.pid_w = 0.0
    }

    if (msg.error_w !== undefined) {
      resolved.error_w = msg.error_w;
    }
    else {
      resolved.error_w = 0.0
    }

    if (msg.total_error_w !== undefined) {
      resolved.total_error_w = msg.total_error_w;
    }
    else {
      resolved.total_error_w = 0.0
    }

    if (msg.d_error_w !== undefined) {
      resolved.d_error_w = msg.d_error_w;
    }
    else {
      resolved.d_error_w = 0.0
    }

    return resolved;
    }
};

module.exports = my_pid;
