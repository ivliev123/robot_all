
"use strict";

let CollisionDetectionState = require('./CollisionDetectionState.js');
let HeadPanCommand = require('./HeadPanCommand.js');
let AnalogIOStates = require('./AnalogIOStates.js');
let NavigatorStates = require('./NavigatorStates.js');
let CameraSettings = require('./CameraSettings.js');
let NavigatorState = require('./NavigatorState.js');
let AssemblyStates = require('./AssemblyStates.js');
let URDFConfiguration = require('./URDFConfiguration.js');
let AnalogIOState = require('./AnalogIOState.js');
let RobustControllerStatus = require('./RobustControllerStatus.js');
let HeadState = require('./HeadState.js');
let CollisionAvoidanceState = require('./CollisionAvoidanceState.js');
let EndpointStates = require('./EndpointStates.js');
let DigitalOutputCommand = require('./DigitalOutputCommand.js');
let SEAJointState = require('./SEAJointState.js');
let DigitalIOStates = require('./DigitalIOStates.js');
let EndpointState = require('./EndpointState.js');
let DigitalIOState = require('./DigitalIOState.js');
let EndEffectorProperties = require('./EndEffectorProperties.js');
let AssemblyState = require('./AssemblyState.js');
let EndEffectorCommand = require('./EndEffectorCommand.js');
let CameraControl = require('./CameraControl.js');
let AnalogOutputCommand = require('./AnalogOutputCommand.js');
let EndEffectorState = require('./EndEffectorState.js');
let JointCommand = require('./JointCommand.js');

module.exports = {
  CollisionDetectionState: CollisionDetectionState,
  HeadPanCommand: HeadPanCommand,
  AnalogIOStates: AnalogIOStates,
  NavigatorStates: NavigatorStates,
  CameraSettings: CameraSettings,
  NavigatorState: NavigatorState,
  AssemblyStates: AssemblyStates,
  URDFConfiguration: URDFConfiguration,
  AnalogIOState: AnalogIOState,
  RobustControllerStatus: RobustControllerStatus,
  HeadState: HeadState,
  CollisionAvoidanceState: CollisionAvoidanceState,
  EndpointStates: EndpointStates,
  DigitalOutputCommand: DigitalOutputCommand,
  SEAJointState: SEAJointState,
  DigitalIOStates: DigitalIOStates,
  EndpointState: EndpointState,
  DigitalIOState: DigitalIOState,
  EndEffectorProperties: EndEffectorProperties,
  AssemblyState: AssemblyState,
  EndEffectorCommand: EndEffectorCommand,
  CameraControl: CameraControl,
  AnalogOutputCommand: AnalogOutputCommand,
  EndEffectorState: EndEffectorState,
  JointCommand: JointCommand,
};
