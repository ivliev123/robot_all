
(cl:in-package :asdf)

(defsystem "aruco_move-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :actionlib_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "aruco_moveAction" :depends-on ("_package_aruco_moveAction"))
    (:file "_package_aruco_moveAction" :depends-on ("_package"))
    (:file "aruco_moveActionFeedback" :depends-on ("_package_aruco_moveActionFeedback"))
    (:file "_package_aruco_moveActionFeedback" :depends-on ("_package"))
    (:file "aruco_moveActionGoal" :depends-on ("_package_aruco_moveActionGoal"))
    (:file "_package_aruco_moveActionGoal" :depends-on ("_package"))
    (:file "aruco_moveActionResult" :depends-on ("_package_aruco_moveActionResult"))
    (:file "_package_aruco_moveActionResult" :depends-on ("_package"))
    (:file "aruco_moveFeedback" :depends-on ("_package_aruco_moveFeedback"))
    (:file "_package_aruco_moveFeedback" :depends-on ("_package"))
    (:file "aruco_moveGoal" :depends-on ("_package_aruco_moveGoal"))
    (:file "_package_aruco_moveGoal" :depends-on ("_package"))
    (:file "aruco_moveResult" :depends-on ("_package_aruco_moveResult"))
    (:file "_package_aruco_moveResult" :depends-on ("_package"))
  ))