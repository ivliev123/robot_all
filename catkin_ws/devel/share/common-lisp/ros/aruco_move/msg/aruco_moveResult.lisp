; Auto-generated. Do not edit!


(cl:in-package aruco_move-msg)


;//! \htmlinclude aruco_moveResult.msg.html

(cl:defclass <aruco_moveResult> (roslisp-msg-protocol:ros-message)
  ((sequence
    :reader sequence
    :initarg :sequence
    :type (cl:vector cl:integer)
   :initform (cl:make-array 0 :element-type 'cl:integer :initial-element 0))
   (aruco_id
    :reader aruco_id
    :initarg :aruco_id
    :type cl:integer
    :initform 0))
)

(cl:defclass aruco_moveResult (<aruco_moveResult>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <aruco_moveResult>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'aruco_moveResult)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name aruco_move-msg:<aruco_moveResult> is deprecated: use aruco_move-msg:aruco_moveResult instead.")))

(cl:ensure-generic-function 'sequence-val :lambda-list '(m))
(cl:defmethod sequence-val ((m <aruco_moveResult>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader aruco_move-msg:sequence-val is deprecated.  Use aruco_move-msg:sequence instead.")
  (sequence m))

(cl:ensure-generic-function 'aruco_id-val :lambda-list '(m))
(cl:defmethod aruco_id-val ((m <aruco_moveResult>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader aruco_move-msg:aruco_id-val is deprecated.  Use aruco_move-msg:aruco_id instead.")
  (aruco_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <aruco_moveResult>) ostream)
  "Serializes a message object of type '<aruco_moveResult>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'sequence))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    ))
   (cl:slot-value msg 'sequence))
  (cl:let* ((signed (cl:slot-value msg 'aruco_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <aruco_moveResult>) istream)
  "Deserializes a message object of type '<aruco_moveResult>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'sequence) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'sequence)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296)))))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'aruco_id) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<aruco_moveResult>)))
  "Returns string type for a message object of type '<aruco_moveResult>"
  "aruco_move/aruco_moveResult")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'aruco_moveResult)))
  "Returns string type for a message object of type 'aruco_moveResult"
  "aruco_move/aruco_moveResult")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<aruco_moveResult>)))
  "Returns md5sum for a message object of type '<aruco_moveResult>"
  "974fb18a104145505e3e80f359a63fb4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'aruco_moveResult)))
  "Returns md5sum for a message object of type 'aruco_moveResult"
  "974fb18a104145505e3e80f359a63fb4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<aruco_moveResult>)))
  "Returns full string definition for message of type '<aruco_moveResult>"
  (cl:format cl:nil "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======~%#result definition~%int32[] sequence~%int32 aruco_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'aruco_moveResult)))
  "Returns full string definition for message of type 'aruco_moveResult"
  (cl:format cl:nil "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======~%#result definition~%int32[] sequence~%int32 aruco_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <aruco_moveResult>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'sequence) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <aruco_moveResult>))
  "Converts a ROS message object to a list"
  (cl:list 'aruco_moveResult
    (cl:cons ':sequence (sequence msg))
    (cl:cons ':aruco_id (aruco_id msg))
))
