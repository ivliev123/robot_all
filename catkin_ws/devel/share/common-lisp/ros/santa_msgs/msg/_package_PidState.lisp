(cl:in-package santa_msgs-msg)
(cl:export '(VX_TO-VAL
          VX_TO
          PID_VX-VAL
          PID_VX
          P_V-VAL
          P_V
          I_V-VAL
          I_V
          D_V-VAL
          D_V
          W_TO-VAL
          W_TO
          PID_W-VAL
          PID_W
          P_W-VAL
          P_W
          I_W-VAL
          I_W
          D_W-VAL
          D_W
))