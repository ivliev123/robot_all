; Auto-generated. Do not edit!


(cl:in-package santa_msgs-msg)


;//! \htmlinclude PidState.msg.html

(cl:defclass <PidState> (roslisp-msg-protocol:ros-message)
  ((vx_to
    :reader vx_to
    :initarg :vx_to
    :type cl:float
    :initform 0.0)
   (pid_vx
    :reader pid_vx
    :initarg :pid_vx
    :type cl:float
    :initform 0.0)
   (P_v
    :reader P_v
    :initarg :P_v
    :type cl:float
    :initform 0.0)
   (I_v
    :reader I_v
    :initarg :I_v
    :type cl:float
    :initform 0.0)
   (D_v
    :reader D_v
    :initarg :D_v
    :type cl:float
    :initform 0.0)
   (w_to
    :reader w_to
    :initarg :w_to
    :type cl:float
    :initform 0.0)
   (pid_w
    :reader pid_w
    :initarg :pid_w
    :type cl:float
    :initform 0.0)
   (P_w
    :reader P_w
    :initarg :P_w
    :type cl:float
    :initform 0.0)
   (I_w
    :reader I_w
    :initarg :I_w
    :type cl:float
    :initform 0.0)
   (D_w
    :reader D_w
    :initarg :D_w
    :type cl:float
    :initform 0.0))
)

(cl:defclass PidState (<PidState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PidState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PidState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name santa_msgs-msg:<PidState> is deprecated: use santa_msgs-msg:PidState instead.")))

(cl:ensure-generic-function 'vx_to-val :lambda-list '(m))
(cl:defmethod vx_to-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:vx_to-val is deprecated.  Use santa_msgs-msg:vx_to instead.")
  (vx_to m))

(cl:ensure-generic-function 'pid_vx-val :lambda-list '(m))
(cl:defmethod pid_vx-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:pid_vx-val is deprecated.  Use santa_msgs-msg:pid_vx instead.")
  (pid_vx m))

(cl:ensure-generic-function 'P_v-val :lambda-list '(m))
(cl:defmethod P_v-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:P_v-val is deprecated.  Use santa_msgs-msg:P_v instead.")
  (P_v m))

(cl:ensure-generic-function 'I_v-val :lambda-list '(m))
(cl:defmethod I_v-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:I_v-val is deprecated.  Use santa_msgs-msg:I_v instead.")
  (I_v m))

(cl:ensure-generic-function 'D_v-val :lambda-list '(m))
(cl:defmethod D_v-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:D_v-val is deprecated.  Use santa_msgs-msg:D_v instead.")
  (D_v m))

(cl:ensure-generic-function 'w_to-val :lambda-list '(m))
(cl:defmethod w_to-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:w_to-val is deprecated.  Use santa_msgs-msg:w_to instead.")
  (w_to m))

(cl:ensure-generic-function 'pid_w-val :lambda-list '(m))
(cl:defmethod pid_w-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:pid_w-val is deprecated.  Use santa_msgs-msg:pid_w instead.")
  (pid_w m))

(cl:ensure-generic-function 'P_w-val :lambda-list '(m))
(cl:defmethod P_w-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:P_w-val is deprecated.  Use santa_msgs-msg:P_w instead.")
  (P_w m))

(cl:ensure-generic-function 'I_w-val :lambda-list '(m))
(cl:defmethod I_w-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:I_w-val is deprecated.  Use santa_msgs-msg:I_w instead.")
  (I_w m))

(cl:ensure-generic-function 'D_w-val :lambda-list '(m))
(cl:defmethod D_w-val ((m <PidState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader santa_msgs-msg:D_w-val is deprecated.  Use santa_msgs-msg:D_w instead.")
  (D_w m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PidState>) ostream)
  "Serializes a message object of type '<PidState>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vx_to))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pid_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'P_v))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'I_v))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'D_v))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_to))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pid_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'P_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'I_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'D_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PidState>) istream)
  "Deserializes a message object of type '<PidState>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vx_to) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pid_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'P_v) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'I_v) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'D_v) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_to) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pid_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'P_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'I_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'D_w) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PidState>)))
  "Returns string type for a message object of type '<PidState>"
  "santa_msgs/PidState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PidState)))
  "Returns string type for a message object of type 'PidState"
  "santa_msgs/PidState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PidState>)))
  "Returns md5sum for a message object of type '<PidState>"
  "efe518362e45b5e054b6d3e7414a7759")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PidState)))
  "Returns md5sum for a message object of type 'PidState"
  "efe518362e45b5e054b6d3e7414a7759")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PidState>)))
  "Returns full string definition for message of type '<PidState>"
  (cl:format cl:nil "########################################~%# Messages~%########################################~%float32 vx_to~%float32 pid_vx~%~%float32 P_v~%float32 I_v~%float32 D_v~%~%float32 w_to~%float32 pid_w~%~%float32 P_w~%float32 I_w~%float32 D_w~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PidState)))
  "Returns full string definition for message of type 'PidState"
  (cl:format cl:nil "########################################~%# Messages~%########################################~%float32 vx_to~%float32 pid_vx~%~%float32 P_v~%float32 I_v~%float32 D_v~%~%float32 w_to~%float32 pid_w~%~%float32 P_w~%float32 I_w~%float32 D_w~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PidState>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PidState>))
  "Converts a ROS message object to a list"
  (cl:list 'PidState
    (cl:cons ':vx_to (vx_to msg))
    (cl:cons ':pid_vx (pid_vx msg))
    (cl:cons ':P_v (P_v msg))
    (cl:cons ':I_v (I_v msg))
    (cl:cons ':D_v (D_v msg))
    (cl:cons ':w_to (w_to msg))
    (cl:cons ':pid_w (pid_w msg))
    (cl:cons ':P_w (P_w msg))
    (cl:cons ':I_w (I_w msg))
    (cl:cons ':D_w (D_w msg))
))
