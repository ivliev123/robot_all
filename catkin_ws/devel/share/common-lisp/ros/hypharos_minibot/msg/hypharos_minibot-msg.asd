
(cl:in-package :asdf)

(defsystem "hypharos_minibot-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "my_pid" :depends-on ("_package_my_pid"))
    (:file "_package_my_pid" :depends-on ("_package"))
  ))