; Auto-generated. Do not edit!


(cl:in-package hypharos_minibot-msg)


;//! \htmlinclude my_pid.msg.html

(cl:defclass <my_pid> (roslisp-msg-protocol:ros-message)
  ((vx_to
    :reader vx_to
    :initarg :vx_to
    :type cl:float
    :initform 0.0)
   (pid_vx
    :reader pid_vx
    :initarg :pid_vx
    :type cl:float
    :initform 0.0)
   (error_vx
    :reader error_vx
    :initarg :error_vx
    :type cl:float
    :initform 0.0)
   (total_error_vx
    :reader total_error_vx
    :initarg :total_error_vx
    :type cl:float
    :initform 0.0)
   (d_error_vx
    :reader d_error_vx
    :initarg :d_error_vx
    :type cl:float
    :initform 0.0)
   (w_to
    :reader w_to
    :initarg :w_to
    :type cl:float
    :initform 0.0)
   (pid_w
    :reader pid_w
    :initarg :pid_w
    :type cl:float
    :initform 0.0)
   (error_w
    :reader error_w
    :initarg :error_w
    :type cl:float
    :initform 0.0)
   (total_error_w
    :reader total_error_w
    :initarg :total_error_w
    :type cl:float
    :initform 0.0)
   (d_error_w
    :reader d_error_w
    :initarg :d_error_w
    :type cl:float
    :initform 0.0))
)

(cl:defclass my_pid (<my_pid>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <my_pid>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'my_pid)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hypharos_minibot-msg:<my_pid> is deprecated: use hypharos_minibot-msg:my_pid instead.")))

(cl:ensure-generic-function 'vx_to-val :lambda-list '(m))
(cl:defmethod vx_to-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:vx_to-val is deprecated.  Use hypharos_minibot-msg:vx_to instead.")
  (vx_to m))

(cl:ensure-generic-function 'pid_vx-val :lambda-list '(m))
(cl:defmethod pid_vx-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:pid_vx-val is deprecated.  Use hypharos_minibot-msg:pid_vx instead.")
  (pid_vx m))

(cl:ensure-generic-function 'error_vx-val :lambda-list '(m))
(cl:defmethod error_vx-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:error_vx-val is deprecated.  Use hypharos_minibot-msg:error_vx instead.")
  (error_vx m))

(cl:ensure-generic-function 'total_error_vx-val :lambda-list '(m))
(cl:defmethod total_error_vx-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:total_error_vx-val is deprecated.  Use hypharos_minibot-msg:total_error_vx instead.")
  (total_error_vx m))

(cl:ensure-generic-function 'd_error_vx-val :lambda-list '(m))
(cl:defmethod d_error_vx-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:d_error_vx-val is deprecated.  Use hypharos_minibot-msg:d_error_vx instead.")
  (d_error_vx m))

(cl:ensure-generic-function 'w_to-val :lambda-list '(m))
(cl:defmethod w_to-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:w_to-val is deprecated.  Use hypharos_minibot-msg:w_to instead.")
  (w_to m))

(cl:ensure-generic-function 'pid_w-val :lambda-list '(m))
(cl:defmethod pid_w-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:pid_w-val is deprecated.  Use hypharos_minibot-msg:pid_w instead.")
  (pid_w m))

(cl:ensure-generic-function 'error_w-val :lambda-list '(m))
(cl:defmethod error_w-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:error_w-val is deprecated.  Use hypharos_minibot-msg:error_w instead.")
  (error_w m))

(cl:ensure-generic-function 'total_error_w-val :lambda-list '(m))
(cl:defmethod total_error_w-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:total_error_w-val is deprecated.  Use hypharos_minibot-msg:total_error_w instead.")
  (total_error_w m))

(cl:ensure-generic-function 'd_error_w-val :lambda-list '(m))
(cl:defmethod d_error_w-val ((m <my_pid>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hypharos_minibot-msg:d_error_w-val is deprecated.  Use hypharos_minibot-msg:d_error_w instead.")
  (d_error_w m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <my_pid>) ostream)
  "Serializes a message object of type '<my_pid>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vx_to))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pid_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'error_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'total_error_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'd_error_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_to))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pid_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'error_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'total_error_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'd_error_w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <my_pid>) istream)
  "Deserializes a message object of type '<my_pid>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vx_to) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pid_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'error_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'total_error_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'd_error_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_to) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pid_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'error_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'total_error_w) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'd_error_w) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<my_pid>)))
  "Returns string type for a message object of type '<my_pid>"
  "hypharos_minibot/my_pid")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'my_pid)))
  "Returns string type for a message object of type 'my_pid"
  "hypharos_minibot/my_pid")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<my_pid>)))
  "Returns md5sum for a message object of type '<my_pid>"
  "2c7179894e731f545124da7ece67825e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'my_pid)))
  "Returns md5sum for a message object of type 'my_pid"
  "2c7179894e731f545124da7ece67825e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<my_pid>)))
  "Returns full string definition for message of type '<my_pid>"
  (cl:format cl:nil "float32 vx_to~%float32 pid_vx~%~%float32 error_vx~%float32 total_error_vx~%float32 d_error_vx~%~%~%float32 w_to~%float32 pid_w~%~%float32 error_w~%float32 total_error_w~%float32 d_error_w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'my_pid)))
  "Returns full string definition for message of type 'my_pid"
  (cl:format cl:nil "float32 vx_to~%float32 pid_vx~%~%float32 error_vx~%float32 total_error_vx~%float32 d_error_vx~%~%~%float32 w_to~%float32 pid_w~%~%float32 error_w~%float32 total_error_w~%float32 d_error_w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <my_pid>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <my_pid>))
  "Converts a ROS message object to a list"
  (cl:list 'my_pid
    (cl:cons ':vx_to (vx_to msg))
    (cl:cons ':pid_vx (pid_vx msg))
    (cl:cons ':error_vx (error_vx msg))
    (cl:cons ':total_error_vx (total_error_vx msg))
    (cl:cons ':d_error_vx (d_error_vx msg))
    (cl:cons ':w_to (w_to msg))
    (cl:cons ':pid_w (pid_w msg))
    (cl:cons ':error_w (error_w msg))
    (cl:cons ':total_error_w (total_error_w msg))
    (cl:cons ':d_error_w (d_error_w msg))
))
