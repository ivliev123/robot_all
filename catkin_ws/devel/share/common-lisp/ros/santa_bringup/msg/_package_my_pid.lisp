(cl:in-package santa_bringup-msg)
(cl:export '(VX_TO-VAL
          VX_TO
          PID_VX-VAL
          PID_VX
          ERROR_VX-VAL
          ERROR_VX
          TOTAL_ERROR_VX-VAL
          TOTAL_ERROR_VX
          D_ERROR_VX-VAL
          D_ERROR_VX
          W_TO-VAL
          W_TO
          PID_W-VAL
          PID_W
          ERROR_W-VAL
          ERROR_W
          TOTAL_ERROR_W-VAL
          TOTAL_ERROR_W
          D_ERROR_W-VAL
          D_ERROR_W
))