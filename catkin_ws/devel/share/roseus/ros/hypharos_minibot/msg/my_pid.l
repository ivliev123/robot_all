;; Auto-generated. Do not edit!


(when (boundp 'hypharos_minibot::my_pid)
  (if (not (find-package "HYPHAROS_MINIBOT"))
    (make-package "HYPHAROS_MINIBOT"))
  (shadow 'my_pid (find-package "HYPHAROS_MINIBOT")))
(unless (find-package "HYPHAROS_MINIBOT::MY_PID")
  (make-package "HYPHAROS_MINIBOT::MY_PID"))

(in-package "ROS")
;;//! \htmlinclude my_pid.msg.html


(defclass hypharos_minibot::my_pid
  :super ros::object
  :slots (_vx_to _pid_vx _error_vx _total_error_vx _d_error_vx _w_to _pid_w _error_w _total_error_w _d_error_w ))

(defmethod hypharos_minibot::my_pid
  (:init
   (&key
    ((:vx_to __vx_to) 0.0)
    ((:pid_vx __pid_vx) 0.0)
    ((:error_vx __error_vx) 0.0)
    ((:total_error_vx __total_error_vx) 0.0)
    ((:d_error_vx __d_error_vx) 0.0)
    ((:w_to __w_to) 0.0)
    ((:pid_w __pid_w) 0.0)
    ((:error_w __error_w) 0.0)
    ((:total_error_w __total_error_w) 0.0)
    ((:d_error_w __d_error_w) 0.0)
    )
   (send-super :init)
   (setq _vx_to (float __vx_to))
   (setq _pid_vx (float __pid_vx))
   (setq _error_vx (float __error_vx))
   (setq _total_error_vx (float __total_error_vx))
   (setq _d_error_vx (float __d_error_vx))
   (setq _w_to (float __w_to))
   (setq _pid_w (float __pid_w))
   (setq _error_w (float __error_w))
   (setq _total_error_w (float __total_error_w))
   (setq _d_error_w (float __d_error_w))
   self)
  (:vx_to
   (&optional __vx_to)
   (if __vx_to (setq _vx_to __vx_to)) _vx_to)
  (:pid_vx
   (&optional __pid_vx)
   (if __pid_vx (setq _pid_vx __pid_vx)) _pid_vx)
  (:error_vx
   (&optional __error_vx)
   (if __error_vx (setq _error_vx __error_vx)) _error_vx)
  (:total_error_vx
   (&optional __total_error_vx)
   (if __total_error_vx (setq _total_error_vx __total_error_vx)) _total_error_vx)
  (:d_error_vx
   (&optional __d_error_vx)
   (if __d_error_vx (setq _d_error_vx __d_error_vx)) _d_error_vx)
  (:w_to
   (&optional __w_to)
   (if __w_to (setq _w_to __w_to)) _w_to)
  (:pid_w
   (&optional __pid_w)
   (if __pid_w (setq _pid_w __pid_w)) _pid_w)
  (:error_w
   (&optional __error_w)
   (if __error_w (setq _error_w __error_w)) _error_w)
  (:total_error_w
   (&optional __total_error_w)
   (if __total_error_w (setq _total_error_w __total_error_w)) _total_error_w)
  (:d_error_w
   (&optional __d_error_w)
   (if __d_error_w (setq _d_error_w __d_error_w)) _d_error_w)
  (:serialization-length
   ()
   (+
    ;; float32 _vx_to
    4
    ;; float32 _pid_vx
    4
    ;; float32 _error_vx
    4
    ;; float32 _total_error_vx
    4
    ;; float32 _d_error_vx
    4
    ;; float32 _w_to
    4
    ;; float32 _pid_w
    4
    ;; float32 _error_w
    4
    ;; float32 _total_error_w
    4
    ;; float32 _d_error_w
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _vx_to
       (sys::poke _vx_to (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pid_vx
       (sys::poke _pid_vx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _error_vx
       (sys::poke _error_vx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _total_error_vx
       (sys::poke _total_error_vx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _d_error_vx
       (sys::poke _d_error_vx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_to
       (sys::poke _w_to (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pid_w
       (sys::poke _pid_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _error_w
       (sys::poke _error_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _total_error_w
       (sys::poke _total_error_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _d_error_w
       (sys::poke _d_error_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _vx_to
     (setq _vx_to (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pid_vx
     (setq _pid_vx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _error_vx
     (setq _error_vx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _total_error_vx
     (setq _total_error_vx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _d_error_vx
     (setq _d_error_vx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_to
     (setq _w_to (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pid_w
     (setq _pid_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _error_w
     (setq _error_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _total_error_w
     (setq _total_error_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _d_error_w
     (setq _d_error_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hypharos_minibot::my_pid :md5sum-) "2c7179894e731f545124da7ece67825e")
(setf (get hypharos_minibot::my_pid :datatype-) "hypharos_minibot/my_pid")
(setf (get hypharos_minibot::my_pid :definition-)
      "float32 vx_to
float32 pid_vx

float32 error_vx
float32 total_error_vx
float32 d_error_vx


float32 w_to
float32 pid_w

float32 error_w
float32 total_error_w
float32 d_error_w

")



(provide :hypharos_minibot/my_pid "2c7179894e731f545124da7ece67825e")


