;; Auto-generated. Do not edit!


(when (boundp 'santa_msgs::PidState)
  (if (not (find-package "SANTA_MSGS"))
    (make-package "SANTA_MSGS"))
  (shadow 'PidState (find-package "SANTA_MSGS")))
(unless (find-package "SANTA_MSGS::PIDSTATE")
  (make-package "SANTA_MSGS::PIDSTATE"))

(in-package "ROS")
;;//! \htmlinclude PidState.msg.html


(defclass santa_msgs::PidState
  :super ros::object
  :slots (_vx_to _pid_vx _P_v _I_v _D_v _w_to _pid_w _P_w _I_w _D_w ))

(defmethod santa_msgs::PidState
  (:init
   (&key
    ((:vx_to __vx_to) 0.0)
    ((:pid_vx __pid_vx) 0.0)
    ((:P_v __P_v) 0.0)
    ((:I_v __I_v) 0.0)
    ((:D_v __D_v) 0.0)
    ((:w_to __w_to) 0.0)
    ((:pid_w __pid_w) 0.0)
    ((:P_w __P_w) 0.0)
    ((:I_w __I_w) 0.0)
    ((:D_w __D_w) 0.0)
    )
   (send-super :init)
   (setq _vx_to (float __vx_to))
   (setq _pid_vx (float __pid_vx))
   (setq _P_v (float __P_v))
   (setq _I_v (float __I_v))
   (setq _D_v (float __D_v))
   (setq _w_to (float __w_to))
   (setq _pid_w (float __pid_w))
   (setq _P_w (float __P_w))
   (setq _I_w (float __I_w))
   (setq _D_w (float __D_w))
   self)
  (:vx_to
   (&optional __vx_to)
   (if __vx_to (setq _vx_to __vx_to)) _vx_to)
  (:pid_vx
   (&optional __pid_vx)
   (if __pid_vx (setq _pid_vx __pid_vx)) _pid_vx)
  (:P_v
   (&optional __P_v)
   (if __P_v (setq _P_v __P_v)) _P_v)
  (:I_v
   (&optional __I_v)
   (if __I_v (setq _I_v __I_v)) _I_v)
  (:D_v
   (&optional __D_v)
   (if __D_v (setq _D_v __D_v)) _D_v)
  (:w_to
   (&optional __w_to)
   (if __w_to (setq _w_to __w_to)) _w_to)
  (:pid_w
   (&optional __pid_w)
   (if __pid_w (setq _pid_w __pid_w)) _pid_w)
  (:P_w
   (&optional __P_w)
   (if __P_w (setq _P_w __P_w)) _P_w)
  (:I_w
   (&optional __I_w)
   (if __I_w (setq _I_w __I_w)) _I_w)
  (:D_w
   (&optional __D_w)
   (if __D_w (setq _D_w __D_w)) _D_w)
  (:serialization-length
   ()
   (+
    ;; float32 _vx_to
    4
    ;; float32 _pid_vx
    4
    ;; float32 _P_v
    4
    ;; float32 _I_v
    4
    ;; float32 _D_v
    4
    ;; float32 _w_to
    4
    ;; float32 _pid_w
    4
    ;; float32 _P_w
    4
    ;; float32 _I_w
    4
    ;; float32 _D_w
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _vx_to
       (sys::poke _vx_to (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pid_vx
       (sys::poke _pid_vx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _P_v
       (sys::poke _P_v (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _I_v
       (sys::poke _I_v (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _D_v
       (sys::poke _D_v (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_to
       (sys::poke _w_to (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pid_w
       (sys::poke _pid_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _P_w
       (sys::poke _P_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _I_w
       (sys::poke _I_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _D_w
       (sys::poke _D_w (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _vx_to
     (setq _vx_to (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pid_vx
     (setq _pid_vx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _P_v
     (setq _P_v (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _I_v
     (setq _I_v (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _D_v
     (setq _D_v (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_to
     (setq _w_to (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pid_w
     (setq _pid_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _P_w
     (setq _P_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _I_w
     (setq _I_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _D_w
     (setq _D_w (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get santa_msgs::PidState :md5sum-) "efe518362e45b5e054b6d3e7414a7759")
(setf (get santa_msgs::PidState :datatype-) "santa_msgs/PidState")
(setf (get santa_msgs::PidState :definition-)
      "########################################
# Messages
########################################
float32 vx_to
float32 pid_vx

float32 P_v
float32 I_v
float32 D_v

float32 w_to
float32 pid_w

float32 P_w
float32 I_w
float32 D_w
")



(provide :santa_msgs/PidState "efe518362e45b5e054b6d3e7414a7759")


